import assert, { deepStrictEqual, strictEqual } from "assert";
import { filterData, checkAgeRange } from "../src/filter.js";
import users from "./mockUser.js";

describe("filterData", function () {

  it("should return all users when no query parameters are provided", function () {
    const filteredUsers = filterData(users, new URLSearchParams(""));
    deepStrictEqual(filteredUsers, users);
  });

  it("should filter users by eye color", function () {
    const filteredUsers = filterData(users, new URLSearchParams("eyeColor=blue"));
    for (const user of filteredUsers) {
      strictEqual(user.eyeColor, "blue");
    }
  });

  it("should filter users by age range", function () {
    const filteredUsers = filterData(users, new URLSearchParams("age=30-40"));
    for (const user of filteredUsers) {
      const age = user.age;
      assert(age >= 30 && age <= 40);
    }
  });

  it("should filter users by multiple query parameters", function () {
    const filteredUsers = filterData(users, new URLSearchParams("eyeColor=blue&age=30-40"));
    for (const user of filteredUsers) {
      strictEqual(user.eyeColor, "blue");
      const age = user.age;
      assert(age >= 30 && age <= 40);
    }
  });

  it("should return an empty array when no users match the criteria", function () {
    const filteredUsers = filterData(users, new URLSearchParams("eyeColor=pink"));
    deepStrictEqual(filteredUsers, []);
  });

  it("should ignore invalid query parameters", function () {
    const filteredUsers = filterData(users, new URLSearchParams("qejn,tjqZ"));
    deepStrictEqual(filteredUsers, users);
  });

  it("should filter users by various eye colors and age ranges", function () {
    const filteredUsers1 = filterData(users, new URLSearchParams("eyeColor=blue"));
    for (const user of filteredUsers1) {
      strictEqual(user.eyeColor, "blue");
    }
    const filteredUsers2 = filterData(users, new URLSearchParams("eyeColor=brown&age=20-30"));
    for (const user of filteredUsers2) {
      strictEqual(user.eyeColor, "brown");
      const age = user.age;
      assert(age >= 20 && age <= 30);
    }
  });

});

describe("checkAgeRange", function () {

  it("should return true for ages within the range", function () {
    strictEqual(checkAgeRange(35, "30-40"), true);
  });

  it("should return false for ages outside the range", function () {
    strictEqual(checkAgeRange(25, "30-40"), false);
  });

  it("should return true for ages within different age ranges", function () {
    strictEqual(checkAgeRange(35, "30-40"), true);
    strictEqual(checkAgeRange(25, "20-30"), true);
  });

  it("should return true for ages at the lower and upper bounds of the range", function () {
    strictEqual(checkAgeRange(30, "30-40"), true);
    strictEqual(checkAgeRange(40, "30-40"), true);
  });

  it("should return false for invalid or improperly formatted age ranges", function () {
    strictEqual(checkAgeRange(35, "qetjqh"), false);
    strictEqual(checkAgeRange(35, "30-20"), false);
  });

});
