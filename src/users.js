import data from "../data/datas.json";

// Map the data to create an array of user objects
export const users = data.map((user) => ({
  _id: user._id,
  index: user.index,
  guid: user.guid,
  isActive: user.isActive,
  balance: user.balance,
  picture: user.picture,
  age: user.age,
  eyeColor: user.eyeColor,
  name: {
    first: user.name.first,
    last: user.name.last,
  },
  company: user.company,
  email: user.email,
  phone: user.phone,
  address: user.address,
  about: user.about,
  registered: user.registered,
  latitude: user.latitude,
  longitude: user.longitude,
  tags: [...user.tags],
  range: [...user.range],
  friends: [...user.friends],
  greeting: user.greeting,
  favoriteFruit: user.favoriteFruit,
}));

// Export the array of user objects
export default users;
