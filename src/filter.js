/**
 * Filter user data based on query parameters
 * @param {Array} data Data to filter
 * @param {*} urlParams Url data
 * @returns Filtered data
 */
export function filterData(data, urlParams) {
  const selectedEyeColor = urlParams.get("eyeColor") || "All";
  const selectedAgeRange = urlParams.get("age") || "All";

  const filteredData = data.filter((user) => {
    return (
      (selectedEyeColor === "All" || user.eyeColor === selectedEyeColor) &&
      (selectedAgeRange === "All" || checkAgeRange(user.age, selectedAgeRange))
    );
  });

  // Return an empty array if nothing matches
  return filteredData.length > 0 ? filteredData : [];
}

/**
 * Check if the age is within the selected range
 * @param {number} age User's age
 * @param {string} selectedRange Selected age range
 * @returns {boolean} True if the age is within the range, false if its not
 */
export function checkAgeRange(age, selectedRange) {
  const [min, max] = selectedRange.split("-");
  return age >= parseInt(min) && age <= parseInt(max);
}
