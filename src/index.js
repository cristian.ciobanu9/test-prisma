import "./style/index.css";
import users from "./users.js";
import { filterData } from "./filter.js";

// Get the table body element
const tableBody = document.getElementById("data-body");

// Keep track of the selected row
let selectedRow = null;

/**
 * Display user data in an HTML table
 * @param {Array} data An array of user data to display
 */
function displayData(data) {
  tableBody.innerHTML = "";

  data.forEach((user) => {
    const row = tableBody.insertRow();

    // Create the cell for isActive and add class for the colored circle
    const isActiveCell = row.insertCell(0);
    const isActiveCircle = document.createElement("div");
    isActiveCircle.classList.add(
      "status-circle",
      user.isActive ? "active" : "inactive"
    );
    isActiveCell.appendChild(isActiveCircle);

    // Add the other cells
    row.insertCell(1).textContent = user.name.last;
    row.insertCell(2).textContent = user.name.first;
    row.insertCell(3).textContent = user.age;
    row.insertCell(4).textContent = user.eyeColor;
    row.insertCell(5).textContent = user.favoriteFruit;
    row.insertCell(6).textContent = user.email;
    row.insertCell(7).textContent = user.phone;

    // Add a click event listener to each row
    row.addEventListener("click", () => addNestedTable(user, row));
  });
}

/**
 * Display additional information about the user when a row is clicked
 * @param {Object} user The user object
 * @param {HTMLTableRowElement} clickedRow The row that was clicked
 */
function addNestedTable(user, clickedRow) {
  if (selectedRow === clickedRow) {
    // If the same row is clicked again, close it
    selectedRow.nextElementSibling.remove();
    selectedRow.classList.remove("highlighted-row");
    selectedRow = null;
    return;
  }

  // Close the selected row ?
  if (selectedRow) {
    selectedRow.nextElementSibling.remove();
    selectedRow.classList.remove("highlighted-row");
  }

  // Create a new row below the clicled row
  const newRow = tableBody.insertRow(clickedRow.rowIndex);

  // Create a cell across all columns
  const cell = newRow.insertCell(0);
  cell.colSpan = clickedRow.cells.length;

  // Create the nested table
  const nestedTable = document.createElement("table");
  nestedTable.className = "nested-table";

  // Function to create a new row
  function createRow(label, content) {
    const row = nestedTable.insertRow();
    row.insertCell(0).textContent = label;
    row.insertCell(1).textContent = content;
  }

  // Add a row for the user's picture
  const pictureRow = nestedTable.insertRow();
  const pictureCell = pictureRow.insertCell(0);
  const userPicture = document.createElement("img");
  userPicture.src = user.picture;
  pictureCell.appendChild(userPicture);

  // Create rows for the other informations
  createRow("Balance:", user.balance);
  createRow("Address:", user.address);
  createRow("About:", user.about);
  createRow("Company:", user.company);

  // Add the nested table to the cell
  cell.appendChild(nestedTable);

  // Add the highlighted class to the clicked row
  clickedRow.classList.add("highlighted-row");
  cell.classList.add("highlighted-content");

  // Update the selected row
  selectedRow = clickedRow;
}

/**
 * Apply & display the filtered results about the users data
 */
function applyFilters() {
  const url = new URLSearchParams(window.location.search);
  const filteredData = filterData(users, url);
  displayData(filteredData);
}

applyFilters();
