# README

This project allows you to filter and display user data based on criteria such as eye color and age range.

## Table of Contents
1. [Installation](#installation)
2. [Usage](#usage)

## Installation

To run the project locally do the following:

1. **Install the packages** 
```bash
npm i
```
2. **Start the server** 
```bash
npm run start
```
This command will start a development server and open the application in your web browser at http://localhost:8080.

3. **Build the project** 
```bash
npm run build
```
This will generate optimized production code in the dist/ directory. Tests will be executing beforehand, and the build will proceed only if they all pass.

Tests can be run independently using the following command:
```bash
npm run test
```

## Usage

**Query Parameters**

The project allows you to filter user data based on query parameters in the URL. You can set the following query parameters:
```
eyeColor: Filter users by eye color (ex: eyeColor=blue).
age: Filter users by age range (ex: age=30-40).
```
**Interactive Features**

- **Filtering**: The user interface allows you to filter users by eye color and age range by specifying query parameters in the URL.

- **View Additional** Information: Click on a user row to display additional information about that user, including a picture, balance, address, about, and company.